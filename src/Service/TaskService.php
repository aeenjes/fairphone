<?php

namespace App\Service;

use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;

class TaskService
{
    /** @var TaskRepository */
    private $taskRepository;

    /**
     * TaskService constructor.
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function getTasksByUser(User $user)
    {
        return $this->taskRepository->findTasksByUser($user);
    }

    public function getAllTasks()
    {
        return $this->taskRepository->findAllTasks();
    }

    public function createTask(Task $task, User $user)
    {
        $this->taskRepository->create($task, $user);
    }

    public function editTask(Task $task)
    {
        $this->taskRepository->edit($task);
    }

    public function deleteTask(Task $task)
    {
        $this->taskRepository->delete($task);
    }
}