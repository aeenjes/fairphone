<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskType;
use App\Service\TaskService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AppController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class AppController extends AbstractController
{
    /** @var TaskService */
    private $taskService;

    /**
     * AppController constructor.
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function indexAction()
    {
        $tasks = $this->taskService->getTasksByUser($this->getUser());

        if(!$tasks) {
            $this->addFlash('info', '😬 Oops! You haven\'t created any tasks at the moment, let\'s create one!');
        }

        return $this->render('index.html.twig', ['tasks' => $tasks]);
    }

    public function tasksAction()
    {
        $tasks = $this->taskService->getAllTasks();

        if(!$tasks) {
            $this->addFlash('info', '😬 Oops! There are not any created tasks at the moment, let\'s create one!');
        }

        return $this->render('tasks.html.twig', ['tasks' => $tasks]);
    }

    public function createAction(Request $request)
    {
        $task = new Task();

        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->taskService->createTask($task, $this->getUser());

            $this->addFlash('success', '✅ Woohoo! You successfully created a task!');

            return $this->redirectToRoute('index');
        }

        return $this->render('create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function editAction(Request $request, Task $task)
    {
        if($task->getUser() != $this->getUser()) {
            $this->addFlash('danger', '❌ Hold up, You don\'t have the access to edit this task!');

            return $this->redirectToRoute('index');
        }

        $form = $this->createForm(TaskType::class, $task);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->taskService->editTask($task);

            $this->addFlash('success', '✅ Woohoo! You successfully edited a task!');

            return $this->redirectToRoute('index');
        }

        return $this->render('edit.html.twig', [
            'form' => $form->createView(),
            'task' => $task
        ]);
    }

    public function deleteAction(Task $task)
    {
        if($task->getUser() != $this->getUser()) {
            $this->addFlash('danger', '❌ Hold up, You don\'t have the access to delete this task!');

            return $this->redirectToRoute('index');
        }

        $this->taskService->deleteTask($task);

        $this->addFlash('success', '✅ Woohoo! You successfully deleted a task!');

        return $this->redirectToRoute('index');
    }
}