<?php

namespace App\Repository;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findTasksByUser(User $user)
    {
        return $this->createQueryBuilder('t')
            ->where('t.user = :user')
            ->orderBy('t.done')
            ->addOrderBy('t.createdAt', 'DESC')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    public function findAllTasks()
    {
        return $this->createQueryBuilder('t')
            ->orderBy('t.done')
            ->addOrderBy('t.createdAt', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function create(Task $task, User $user)
    {
        $task->setUser($user);
        $task->setDone(false);

        $this->getEntityManager()->persist($task);
        $this->getEntityManager()->flush();
    }

    public function edit(Task $task)
    {
        $this->getEntityManager()->merge($task);
        $this->getEntityManager()->flush();
    }

    public function delete(Task $task)
    {
        $this->getEntityManager()->remove($task);
        $this->getEntityManager()->flush();
    }
}
